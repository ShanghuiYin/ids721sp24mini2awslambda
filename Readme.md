# IDS721 Spring 2024 Weekly Mini Project 2

## Live Demo

Click this [link](https://kwx46mvhx2.execute-api.us-east-1.amazonaws.com/default/lambda-demo?numbers=3,2,1) to access

| Sample output for above url

![alt text](image-1.png)

- You could change the number list to sort any random integer list


## Requirements
1. Rust Lambda Function using Cargo Lambda.
2. Process and transform sample data

## Setup
1. Install Rust with cargo
2. Install AWS Toolkit for VS Code
3. Install Cargo Lambda `brew tap cargo-lambda/cargo-lambda
   brew install cargo-lambda`

## Run
My simple lambda function takes a random integer number list and returns the sorted list.

1. Create the project `cargo lambda new lambda-demo \
   && cd lambda-demo`
2. Serve the function locally for testing `cargo lambda watch`
3. Test `http://localhost:9000/?numbers=3,1,4,1,5,9,2,6"`
4. Output `{"sorted_numbers":[1,1,2,3,4,5,6,9]}`

## Deploy
1. Deploy the function `cargo lambda deploy`
2. Go to the AWS console and you will find your deployed function

As shown in the screenshot below, the function is deployed and invoked successfully.

![AWS Img](image.png)

## AWS API Gateway Trigger

1. Open the AWS Management Console
2. Click on the lambda function service
3. Select and Click the function you want to add the trigger
4. Click on the Add trigger button
5. Choose the API Gateway
6. Choose the Create a new API option
7. Choose the REST API option
8. Choose Security Method _Open_
9. Click on the Add button
10. Access the function using the URL provided in the API Gateway trigger details


## Reference

- https://www.cargo-lambda.info/
- https://docs.aws.amazon.com/toolkit-for-vscode/latest/userguide/welcome.html
