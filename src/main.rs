use lambda_http::{run, service_fn, Error, Request, Response, IntoResponse, Body};
use serde_json::json;
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use std::collections::HashMap;
use serde_qs as qs; // Add serde_qs to your Cargo.toml for parsing

async fn function_handler(event: Request) -> Result<impl IntoResponse, Error> {
    // Extract the query string from the request URI
    let query_string = event.uri().query().unwrap_or("");

    // Parse the query string into a HashMap
    // Assuming you are expecting the query parameter 'numbers' with a list of numbers like 'numbers=1,2,3'
    let params: HashMap<String, String> = qs::from_str(query_string).unwrap_or_else(|_| HashMap::new());
    
    // Convert the 'numbers' query parameter into a Vec<i32>
    let numbers: Vec<i32> = params.get("numbers")
        .and_then(|nums| nums.split(',')
            .map(|n| n.parse::<i32>().ok())
            .collect::<Option<Vec<i32>>>()
        ).unwrap_or_else(|| vec![]);

    // Sort the numbers in ascending order
    let mut sorted_numbers = numbers.clone();
    sorted_numbers.sort();

    // Prepare the sorted list as JSON for the response
    let response_body = json!({ "sorted_numbers": sorted_numbers });

    // Return the JSON response
    let resp = Response::builder()
        .status(200)
        .header("Content-Type", "application/json")
        .body(Body::from(response_body.to_string()))
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with_target(false)
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
