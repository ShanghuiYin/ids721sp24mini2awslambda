watch:
	cargo lambda watch

invoke:
	# this is local command line invoke
	http://localhost:9000/?numbers=3,1,4,1,5,9,2,6

build:
	cargo lambda build --release

deploy:
	cargo lambda deploy
